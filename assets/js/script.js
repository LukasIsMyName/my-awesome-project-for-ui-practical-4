function draw() 
{
  //Gets the canvas element using an ID
  var canvas = document.getElementById("gameCanvas");
  var context = canvas.getContext('2d'); //Gets the tools for 2D drawing

  context.fillStyle = "#49c1e4"; //Sets color
  //Draws a rectangle at 0, 0 coordinates in the x y axes, size of 100 x 100
  context.fillRect(0, 0, 100, 100);
  context.fillRect(0, 400, 100, 100);
  context.fillRect(700, 0, 100, 100);
  context.fillRect(700, 400, 100, 100);

  console.log("Works up to here, line 14"); //Debug

  //Triangle
  context.beginPath();
  context.moveTo(100, 200);
  context.lineTo(300, 200);
  context.lineTo(100, 300);
  context.lineTo(100, 200);
  context.stroke();
  context.fillStyle = "Blue"; //Define fill style
  context.fill(); //Quite literally fill

  console.log("Works up to here, line 24"); //Debug

  //Triangle
  context.beginPath();
  context.moveTo(700, 200);
  context.lineTo(500, 200);
  context.lineTo(700, 300);
  context.lineTo(700, 200);
  context.fillStyle = "Red"; //Define fill style
  context.fill(); //Quite literally fills
  context.stroke();
  

  console.log("Works up to here, line 38");

  //Double triangle
  context.beginPath();
  context.moveTo(400, 250);
  context.lineTo(300, 200);
  context.lineTo(500, 200);
  context.lineTo(400, 250);
  context.lineTo(300, 300);
  context.lineTo(500, 300);
  context.lineTo(400, 250);
  context.fillStyle = "Grey";
  context.fill();
  context.stroke();
  
  console.log("Works up to here, line 54");

  //Text
  context.beginPath();
  context.fillStyle = "Black";
  context.fill();
  context.font = "20px";
  context.fillText("A line of text on a triangle :o", 100, 195);

  context.font = "20px sans-serif";
  context.fillText("Another line of text on a triangle", 400, 195 )

  context.font = "15px arial";
  context.rotate(45);
  context.fillText("Text in an angle", 430, -280);

  console.log("Works up to here, line 69, nice");

  //Image

  var image = new Image();
  image.src = "assets/images/cw.jpg";
  image.onload = function() {

    context.drawImage("image", 500, 400);
  }
}


//Loads the function inside the html
draw();